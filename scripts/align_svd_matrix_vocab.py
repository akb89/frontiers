"""Align all raw count matrices based on shared vocabulary."""
import os
import itertools
import multiprocessing

import embeddix


def reduce(model_filepath):
    print('Reducing {}'.format(os.path.basename(model_filepath)))
    vocab_filepath = '{}.vocab'.format(model_filepath.split('.ppmi')[0])
    model = embeddix.load_dense(model_filepath)
    vocab = embeddix.load_vocab(vocab_filepath)
    reduced = embeddix.reduce_dense(model, vocab, shared_vocab)
    output_model_filepath = '{}.singvectors.aligned'.format(
        os.path.join(os.path.dirname(model_filepath),
                     os.path.basename(model_filepath).split('.singvectors')[0]))
    output_vocab_filepath = '{}.aligned.vocab'.format(
        os.path.join(os.path.dirname(vocab_filepath),
                     os.path.basename(vocab_filepath).split('.vocab')[0]))
    embeddix.save_dense(output_model_filepath, reduced)
    embeddix.save_vocab(output_vocab_filepath, shared_vocab)


if __name__ == '__main__':
    NUM_THREADS = 11  # one per model
    MATRIX_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/svd/'  # replace this with your own env path
    models_filepaths = [os.path.join(MATRIX_DIRPATH, filename) for filename in
                        os.listdir(MATRIX_DIRPATH)
                        if filename.endswith('.singvectors.npy')]
    shared_vocab = {}
    for x, y in itertools.combinations(models_filepaths, 2):
        x_vocab_filepath = '{}.vocab'.format(x.split('.ppmi')[0])
        y_vocab_filepath = '{}.vocab'.format(y.split('.ppmi')[0])
        print('Aligning {} and {}'
              .format(os.path.basename(x_vocab_filepath),
                      os.path.basename(y_vocab_filepath)))
        x_vocab = embeddix.load_vocab(x_vocab_filepath)
        y_vocab = embeddix.load_vocab(y_vocab_filepath)
        x_y_shared_vocab = embeddix.get_shared_vocab(x_vocab, y_vocab)
        if not shared_vocab:
            shared_vocab = x_y_shared_vocab
        else:
            shared_vocab = embeddix.get_shared_vocab(shared_vocab,
                                                     x_y_shared_vocab)
    with multiprocessing.Pool(NUM_THREADS) as pool:
        for _ in pool.imap_unordered(reduce, models_filepaths):
            pass
