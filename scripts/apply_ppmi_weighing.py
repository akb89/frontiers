"""Apply PPMI weighing."""
import os
import multiprocessing
import counterix

if __name__ == '__main__':
    NUM_THREADS = 11  # one per model
    ALIGNED_MODELS_DIRPATH = '/home/debian/frontiers/models/raw/'  # replace this with your own env path
    models_filepaths = [os.path.join(ALIGNED_MODELS_DIRPATH, filename)
                        for filename in os.listdir(ALIGNED_MODELS_DIRPATH) if
                        filename.endswith('.npz')
                        and not filename.endswith('.ppmi.npz')]
    with multiprocessing.Pool(NUM_THREADS) as pool:
        for _ in pool.imap_unordered(counterix.weigh, models_filepaths):
            pass
