"""Generate raw count models for various corpora, excluding full Wikipedia."""
import os
import functools
import multiprocessing

import counterix


def generate(min_count, win_size, corpus_filepath):
    return counterix.generate(corpus_filepath, min_count, win_size)


if __name__ == '__main__':
    MIN_COUNT = 2
    WIN_SIZE = 2
    NUM_THREADS = 6  # one per corpus
    CORPORA_DIRPATH = '/home/debian/corpora/'  # replace this with your own env path
    corpora_filepaths = [os.path.join(CORPORA_DIRPATH, filename) for filename in
                         os.listdir(CORPORA_DIRPATH) if filename.endswith('txt')
                         and filename != 'wiki.lower.tokenized.txt']
    with multiprocessing.Pool(NUM_THREADS) as pool:
        _generate = functools.partial(generate, MIN_COUNT, WIN_SIZE)
        for _ in pool.imap_unordered(_generate, corpora_filepaths):
            pass
