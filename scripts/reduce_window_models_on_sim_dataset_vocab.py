"""Reduce the window models to retain only rows in MEN/SIMLEX/SIMVERB vocab."""
import os
import functools
import multiprocessing

import embeddix


def reduce(sim_dataset_words, model_filepath):
    model = embeddix.load_dense(model_filepath)
    vocab_filepath = '{}.vocab'.format(model_filepath.split('.ppmi')[0])
    vocab = embeddix.load_vocab(vocab_filepath)
    reduced_vocab = embeddix.get_shared_vocab(vocab, sim_dataset_words)
    return model_filepath, embeddix.reduce_dense(model, vocab, reduced_vocab), reduced_vocab

if __name__ == '__main__':
    MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/window/'
    NUM_THREADS = 5
    models_filepaths = [os.path.join(MODELS_DIRPATH, filename) for filename in
                        os.listdir(MODELS_DIRPATH)
                        if filename.endswith('.singvectors.npy')]
    men_words = embeddix.load_dataset_words('men')
    simlex_words = embeddix.load_dataset_words('simlex')
    simverb_words = embeddix.load_dataset_words('simverb')
    all_words = men_words.union(simlex_words, simverb_words)
    with multiprocessing.Pool(NUM_THREADS) as pool:
        _reduce = functools.partial(reduce, all_words)
        for model_filepath, reduced_model, reduced_vocab in pool.imap_unordered(_reduce, models_filepaths):
            reduced_model_filepath = '{}.reduced'.format(model_filepath.split('.npy')[0])
            reduced_vocab_filepath = '{}.reduced.vocab'.format(model_filepath.split('.ppmi')[0])
            embeddix.save_dense(reduced_model_filepath, reduced_model)
            embeddix.save_vocab(reduced_vocab_filepath, reduced_vocab)
