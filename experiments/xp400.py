"""Coordination is an interactive process: RMSE table."""

import os
import functools

import utils
from utils import MyPool


if __name__ == '__main__':
    # MODELS_DIRPATH = '/Users/akb/Gitlab/frontiers/models/aligned_after_svd/'  # set this to your own corresponding directory
    # PAIRS_LIST = ['oanc-wiki07']
    MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/aligned/'  # set this to your own corresponding directory
    PAIRS_LIST = ['oanc-wiki07', 'acl-wiki2', 'bnc-wiki4', 'wiki07-wiki2',
                  'wiki07-wiki4', 'wiki2-wiki4']
    METRIC = 'both'
    NUM_THREADS = 6  # 1 thread per pair
    NUM_RUNS = 10  # for random models only
    TOP = 30  # also serves as limit parameter
    SCALE = 1e4
    results = {}
    models_filepaths = [os.path.join(MODELS_DIRPATH, filename) for filename in
                        os.listdir(MODELS_DIRPATH)
                        if filename.endswith('.singvectors.aligned.npy')]
    with MyPool(NUM_THREADS) as pool:
        _compute_results = functools.partial(
            utils.compute_rmse_on_sampled_dim, METRIC, TOP, NUM_RUNS,
            models_filepaths)
        for pair, _results in pool.imap_unordered(_compute_results,
                                                  PAIRS_LIST):
            results[pair] = _results
    print('RMSE on pairs {}'.format(' & '.join(PAIRS_LIST)))
    print('RANDOM & {}'.format(
        ' & '.join(
            ['{} $\\pm$ {}'.format(round(results[pair]['random']['avg']*SCALE, 2),
                                   round(results[pair]['random']['ste']*SCALE, 2))
             for pair in PAIRS_LIST])))
    print('TOP & {}'.format(' & '.join([str(round(results[pair]['top']*SCALE, 2)) for pair in PAIRS_LIST])))
    print('MEN & {}'.format(' & '.join([str(round(results[pair]['men']*SCALE, 2)) for pair in PAIRS_LIST])))
    print('SIMLEX & {}'.format(' & '.join([str(round(results[pair]['simlex']*SCALE, 2)) for pair in PAIRS_LIST])))
    print('SIMVERB & {}'.format(' & '.join([str(round(results[pair]['simverb']*SCALE, 2)) for pair in PAIRS_LIST])))
