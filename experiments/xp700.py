"""Compute intersection of vocabularies across models."""
import os
import functools

import embeddix

from utils import MyPool


def compare_counts(counts_filepaths, pair):
    name1 = pair.split('-')[0]
    name2 = pair.split('-')[1]
    for counts_filepath in counts_filepaths:
        if os.path.basename(counts_filepath).startswith(name1):
            counts1 = embeddix.load_counts(counts_filepath)
        elif os.path.basename(counts_filepath).startswith(name2):
            counts2 = embeddix.load_counts(counts_filepath)
    total_counts1 = sum(x for x in counts1.values())
    total_counts2 = sum(x for x in counts2.values())
    oov1 = set(word for word in counts1 if word not in counts2)
    oov2 = set(word for word in counts2 if word not in counts1)
    oov1_counts = sum(counts1[x] for x in oov1)
    oov2_counts = sum(counts2[x] for x in oov2)
    return pair, {
        1: {
            'oov_ratio': (len(oov1) / len(counts1)) * 100,
            'oov_counts_ratio': (oov1_counts / total_counts1) * 100
        },
        2: {
            'oov_ratio': (len(oov2) / len(counts2)) * 100,
            'oov_counts_ratio': (oov2_counts / total_counts2) * 100
        }
    }


if __name__ == '__main__':
    COUNTS_DIRPATH = '/home/debian/frontiers/models/counts/'
    PAIRS_LIST = ['oanc-wiki07', 'acl-wiki2', 'bnc-wiki4', 'wiki07-wiki2',
                  'wiki07-wiki4', 'wiki2-wiki4']
    NUM_THREADS = 3  # same as number of pairs
    results = {}
    counts_filepaths = [os.path.join(COUNTS_DIRPATH, filename) for filename in
                        os.listdir(COUNTS_DIRPATH)
                        if filename.endswith('.counts')]
    with MyPool(NUM_THREADS) as pool:
        _compute_results = functools.partial(compare_counts, counts_filepaths)
        for pair, _results in pool.imap_unordered(_compute_results, PAIRS_LIST):
            results[pair] = _results
    print('pair & oov-ratio1 & oov-counts-ratio1 & oov-ratio2 & oov-counts-ratio2')
    for pair in PAIRS_LIST:
        print('{} & {}\% & {}\% & {}\% & {}\%'.format(
            pair, round(results[pair][1]['oov_ratio']),
            round(results[pair][1]['oov_counts_ratio']),
            round(results[pair][2]['oov_ratio']),
            round(results[pair][2]['oov_counts_ratio'])))
