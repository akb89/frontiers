"""Compute energy preservation.

Upon generating the wiki-singvalues.dat file, use the following LaTeX snippet
to plot the singular values distribution:
"""
# \begin{figure}[htpb]
#     \centering
#     \vspace{0.5cm}
#     \resizebox{\linewidth}{!}{
#      \begin{tikzpicture}
#       \begin{axis}[
#        xlabel=DIM,
#        ylabel=Singular values,
#        legend cell align={left},
#        legend style={at={(0.5, 0.5)}, anchor= south west}
#        ]
#        \addplot [color=black] table[x expr=\coordindex+1,y index=0] {data/wiki-singvalues.dat};
#       \end{axis}
#      \end{tikzpicture}
#     }
#     \captionof{figure}{Distribution of singular values across [0, \numprint{10000}] top dimensions for a PPMI-weighted count-based DSM generated on the full English Wikipedia~(\wiki) corpus detailed in Table~\ref{tab:corpora}, with a window of 2.}
#     \label{fig:energy}
#   \end{figure}
import os

import embeddix

if __name__ == '__main__':
    TOP = 300
    # Change below to your own corresponding env dirpath
    SVD_MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/svd/'
    PPMI_MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/ppmi/'
    WIKI_FILEPATH = '/home/debian/frontiers/models/aligned_after_svd/window/wiki.lower.tokenized.mc-30.win-2.ppmi.k-10000.singvalues.npy'
    CORPUS_LIST = ['wiki07', 'oanc', 'wiki2', 'acl', 'wiki4', 'bnc']
    singvalues_filepaths = [os.path.join(SVD_MODELS_DIRPATH, filename)
                            for filename in os.listdir(SVD_MODELS_DIRPATH)
                            if filename.endswith('.singvalues.npy')]
    results = {}
    for singvalue_filepath in singvalues_filepaths:
        corpus_name = os.path.basename(singvalue_filepath).split('.lower')[0]
        singvalues = embeddix.load_dense(singvalue_filepath)
        ppmi_filepath = os.path.join(
            PPMI_MODELS_DIRPATH,
            '{}.npz'.format(os.path.basename(
                singvalue_filepath).split('.k-')[0]))
        ppmi_matrix = embeddix.load_sparse(ppmi_filepath)
        results[corpus_name] = {
            'full': (embeddix.energy(singvalues) / embeddix.energy(ppmi_matrix)) * 100,
            'top': (embeddix.energy(singvalues[:TOP]) / embeddix.energy(ppmi_matrix)) * 100
        }
    wiki_singval = embeddix.load_dense(WIKI_FILEPATH)
    wiki_ppmi_filepath = '{}.npz'.format(WIKI_FILEPATH.split('.k-')[0])
    wiki_ppmi = embeddix.load_sparse(wiki_ppmi_filepath)
    wiki_full_energy = (embeddix.energy(wiki_singval) / embeddix.energy(wiki_ppmi)) * 100
    wiki_top_energy = (embeddix.energy(wiki_singval[:TOP]) / embeddix.energy(wiki_ppmi)) * 100
    for corpus in CORPUS_LIST:
        print('{} & {}\\% & {}\\%'.format(
            corpus, round(results[corpus]['full']),
            round(results[corpus]['top'])))
    print('wiki & {}\\% & {}\\%'.format(round(wiki_full_energy),
                                        round(wiki_top_energy)))
    with open('wiki-singvalues.dat', 'w', encoding='utf-8') as output_str:
        for x in wiki_singval:
            print(x, file=output_str)
