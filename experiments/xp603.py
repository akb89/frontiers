"""Locate the peak of conflict."""
import os
import functools

import numpy as np

import utils
from utils import MyPool


def detect_agreement_peak_dim(bin_size, models_filepaths, pair):
    "Roughly: two consecutive decrease."
    _, idx_rmse_tuples = utils.compute_rmse_on_bins(bin_size, models_filepaths, pair)
    rmses = [rmse for _, rmse in idx_rmse_tuples]
    max_rmse_idx = rmses.index(max(rmses))
    peak = (max_rmse_idx + 1) * bin_size
    min_rmse_in_compat = min(rmses[max_rmse_idx:])
    num_bins_agreement_zone = len(rmses[:max_rmse_idx])
    num_bins_compat_zone = len(rmses[max_rmse_idx:])
    num_bins_agr_below_compat_min_rmse = sum(1 for x in rmses[:max_rmse_idx] if x < min_rmse_in_compat)
    return pair, {
        'peak': peak,
        'max_rmse': max(rmses),
        'min_rmse_in_compat': min_rmse_in_compat,
        'num_bins_agreement_zone': num_bins_agreement_zone,
        'num_bins_compat_zone': num_bins_compat_zone,
        'num_bins_agr_below_compat_min_rmse': num_bins_agr_below_compat_min_rmse
    }



if __name__ == '__main__':
    # MODELS_DIRPATH = '/Users/akb/Gitlab/frontiers/models/aligned_after_svd/'  # set this to your own corresponding directory
    # PAIRS_LIST = ['oanc-wiki07']
    MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/aligned/'  # set this to your own corresponding directory
    # MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_before_svd/svd/'
    PAIRS_LIST = ['oanc-wiki07', 'acl-wiki2', 'bnc-wiki4']
    BIN_SIZE = 30
    SCALE = 1e4
    NUM_THREADS = 3  # same as number of pairs
    results = {}
    models_filepaths = [os.path.join(MODELS_DIRPATH, filename) for filename in
                        os.listdir(MODELS_DIRPATH)
                        if filename.endswith('.singvectors.aligned.npy')]
    with MyPool(NUM_THREADS) as pool:
        _compute_results = functools.partial(
            detect_agreement_peak_dim, BIN_SIZE, models_filepaths)
        for pair, dim in pool.imap_unordered(_compute_results, PAIRS_LIST):
            results[pair] = dim
    print('Peak of conflict dim for pairs')
    for pair in PAIRS_LIST:
        print('{} & {}'.format(pair, results[pair]['peak']))
    print('Max RMSE')
    for pair in PAIRS_LIST:
        print('{} & {}'.format(pair, results[pair]['max_rmse']*SCALE))
    print('Min RMSE value in compatibility zone')
    for pair in PAIRS_LIST:
        print('{} & {}'.format(pair, results[pair]['min_rmse_in_compat']*SCALE))
    print('Number of bins in the agreement zone')
    for pair in PAIRS_LIST:
        print('{} & {}'.format(pair, results[pair]['num_bins_agreement_zone']))
    print('Number of bins in the compatibility zone')
    for pair in PAIRS_LIST:
        print('{} & {}'.format(pair, results[pair]['num_bins_compat_zone']))
    print('Number of bins in the agreement zone below min RMSE in compatibility zone')
    for pair in PAIRS_LIST:
        print('{} & {}'.format(pair, results[pair]['num_bins_agr_below_compat_min_rmse']))
