"""Sampled dimensions on full corpora (no kfold)."""
import os
import functools

import utils
from utils import MyPool


if __name__ == '__main__':
    # MODELS_DIRPATH = '/Users/akb/Gitlab/frontiers/models/aligned_after_svd/'  # set this to your own corresponding directory
    # CORPUS_LIST = ['wiki07', 'oanc']
    MODELS_DIRPATH = '/home/kabbach/frontiers/models/aligned_after_svd/aligned/'  # set this to your own corresponding directory
    CORPUS_LIST = ['wiki07', 'oanc', 'wiki2', 'acl', 'wiki4', 'bnc']
    METRIC = 'both'
    DATASET = 'men'  # change to 'simlex' to run SIMLEX XP
    SAMPLING_MODE = 'seq'
    NUM_THREADS = 6  # 1 thread per model
    NUM_RUNS = 10
    LIMIT = 0  # unused parameter in seq mode
    MODE = 'corpora'
    results = {}
    models_filepaths = [os.path.join(MODELS_DIRPATH, filename) for filename in
                        os.listdir(MODELS_DIRPATH)
                        if filename.endswith('.singvectors.aligned.npy')]
    with MyPool(NUM_THREADS) as pool:
        _compute_results = functools.partial(
            utils.sample_dims, NUM_RUNS, DATASET, SAMPLING_MODE, METRIC, LIMIT, MODE)
        for corpus, _results in pool.imap_unordered(_compute_results,
                                                    models_filepaths):
            results[corpus] = _results
    print('Sampled dims on {} in {} mode'.format(DATASET, SAMPLING_MODE))
    print('CORPUS & median & mean & ninety')
    for corpus in CORPUS_LIST:
        print('{} & {} \\pm {} & {} \\pm {} & {} \\pm {}'.format(
            corpus, results[corpus]['median']['avg'],
            results[corpus]['median']['ste'],
            results[corpus]['mean']['avg'],
            results[corpus]['mean']['ste'],
            results[corpus]['ninety']['avg'],
            results[corpus]['ninety']['ste']))
