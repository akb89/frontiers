"""Beyond similarity: conceptual compatibility."""
import os
import functools

import numpy as np

import utils
from utils import MyPool


def compute_rmse_log_pearson(bin_size, models_filepaths, pair):
    xcorrx = utils.compute_log_pearson(models_filepaths, pair)
    _, idx_rmse_tuples = utils.compute_rmse_on_bins(bin_size, models_filepaths, pair)
    avgs = np.log(utils.binize(np.array(xcorrx), bin_size).mean(axis=1))
    return pair, {
        'rmse': [rmse for _, rmse in idx_rmse_tuples],
        'pearson': avgs
    }


if __name__ == '__main__':
    MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_before_svd/svd/'  # set this to your own corresponding directory
    PAIRS_LIST = ['oanc-wiki07', 'acl-wiki2', 'bnc-wiki4']
    BIN_SIZE = 30
    SCALE = 1e4
    NUM_THREADS = 3  # same as number of pairs
    results = {}
    models_filepaths = [os.path.join(MODELS_DIRPATH, filename) for filename in
                        os.listdir(MODELS_DIRPATH)
                        if filename.endswith('.singvectors.npy')]
    with MyPool(NUM_THREADS) as pool:
        _compute_results = functools.partial(
            compute_rmse_log_pearson, BIN_SIZE, models_filepaths)
        for pair, _results in pool.imap_unordered(_compute_results, PAIRS_LIST):
            results[pair] = _results
    for pair in PAIRS_LIST:
        with open('{}-disc-pearson-log.dat'.format(pair), 'w', encoding='utf-8') as output_str:
            for val in results[pair]['pearson']:
                print('{}'.format(val), file=output_str)
        with open('{}-disc-rmse-pearson.dat'.format(pair), 'w', encoding='utf-8') as output_str:
            for pearson, rmse in zip(results[pair]['pearson'], results[pair]['rmse']):
                print('{}\t{}'.format(pearson, rmse*SCALE), file=output_str)
