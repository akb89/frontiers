"""Quantify overfitting between different metrics.

Measure overfitting as the average gap between train and test Spearman
correlations on the MEN and Simlex datasets, for 10 runs with 5-fold
validation.

Test 4 different metrics: spr, pearson, rmse, both.
both is the combined spr + rmse.

Tests are run only on the full wikipedia model.
"""
import functools

import numpy as np
from scipy import stats

import embeddix
import entropix

from utils import MyPool


def compute_avg_diff(sampling):
    diff = [
        entropix.evaluate_on_splits(model[:, sampling[fold]['dims']],
                                    sampling[fold]['train'], 'spr') -
        entropix.evaluate_on_splits(model[:, sampling[fold]['dims']],
                                    sampling[fold]['test'], 'spr')
        for fold in sampling.keys()]
    test_spr = [
        entropix.evaluate_on_splits(model[:, sampling[fold]['dims']],
                                    sampling[fold]['test'], 'spr')
        for fold in sampling.keys()]
    return diff, test_spr


def compute_overfitting(dataset, vocab, num_runs, kfold_size, max_num_threads,
                        metric):
    diff = []
    perf = []
    for _ in range(num_runs):
        splits_dict = entropix.load_splits_dict(dataset, vocab, kfold_size)
        sampling = entropix.sample(
            model=model, splits_dict=splits_dict, dataset=dataset,
            kfold_size=kfold_size, mode='seq', metric=metric, shuffle=True,
            max_num_threads=max_num_threads, limit=0)
        mdiff, spr = compute_avg_diff(sampling)
        diff.extend(mdiff)
        perf.extend(spr)
    return metric, {
        'spr': {
            'avg': np.mean(perf),
            'ste': stats.sem(perf)
        },
        'diff': {
            'avg': np.mean(diff),
            'ste': stats.sem(diff)
        }
    }


if __name__ == '__main__':
    METRICS = ['spr', 'pearson', 'rmse', 'both']
    DATASET = 'men'
    NUM_THREADS = 4  # one per metric
    NUM_KFOLD_THREADS = 5  # one per fold
    NUM_RUNS = 10
    KFOLD_SIZE = .20  # 20%, i.e. 5-fold validation
    results = {}
    # MODEL_FILEPATH = '/Users/akb/Gitlab/frontiers/models/aligned_after_svd/oanc.lower.tokenized.mc-2.win-2.ppmi.k-10000.singvectors.aligned.npy'
    # VOCAB_FILEPATH = '{}.aligned.vocab'.format(MODEL_FILEPATH.split('.ppmi')[0])
    MODEL_FILEPATH = '/home/kabbach/frontiers/models/aligned_after_svd/window-reduced/wiki.lower.tokenized.mc-30.win-2.ppmi.k-10000.singvectors.reduced.npy'
    VOCAB_FILEPATH = '{}.reduced.vocab'.format(MODEL_FILEPATH.split('.ppmi')[0])
    global model
    model = embeddix.load_dense(MODEL_FILEPATH)
    vocab = embeddix.load_vocab(VOCAB_FILEPATH)
    results = {}
    with MyPool(NUM_THREADS) as pool:
        _compute_results = functools.partial(
            compute_overfitting, DATASET, vocab, NUM_RUNS, KFOLD_SIZE,
            NUM_KFOLD_THREADS)
        for metric, _results in pool.imap_unordered(_compute_results, METRICS):
            results[metric] = _results
    print('Printing overfitting results on {}'.format(DATASET.upper()))
    print('METRICS = {}'.format(' & '.join(METRICS)))
    print('spr diff = {}'.format(
        ' & '.join(
            ['{} $\\pm$ {}'.format(
                round(results[metric]['diff']['avg'], 2),
                round(results[metric]['diff']['ste'], 2))
             for metric in METRICS])))
    print('test spr = {}'.format(
        ' & '.join(
            ['{} $\\pm$ {}'.format(
                round(results[metric]['spr']['avg'], 2),
                round(results[metric]['spr']['ste'], 2))
             for metric in METRICS])))
