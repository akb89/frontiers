"""Compute MEN, SIMLEX and SIMVERB scores on TOP&SEQ models on full wikipedia.

Wikipedia DSMs are generated with different window size = [1, 2, 5, 10, 15]
"""
import os
import functools

import entropix
import embeddix

import utils
from utils import MyPool


if __name__ == '__main__':
    # MODELS_DIRPATH = '/Users/akb/Gitlab/frontiers/models/window/'
    # CORPUS_LIST = ['win-1', 'win-2']
    # NUM_THREADS = 2  # 1 thread per model
    # NUM_RUNS = 2
    MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/window-reduced/'
    CORPUS_LIST = ['win-1', 'win-2', 'win-5', 'win-10', 'win-15']
    METRIC = 'both'
    DATASET = 'men'  # change to 'simlex' to run SIMLEX xp
    NUM_THREADS = 5  # 1 thread per model
    NUM_KFOLD_THREADS = 5  # 1 thread per fold in 5-fold
    NUM_RUNS = 10
    KFOLD_SIZE = .20  # 20%, i.e. 5-fold validation
    results = {}
    samples = {}
    models_filepaths = [os.path.join(MODELS_DIRPATH, filename) for filename in
                        os.listdir(MODELS_DIRPATH)
                        if filename.endswith('.singvectors.reduced.npy')]
    # all models should have the exact same vocab here if generated with counterix
    vocab_filepath = '{}.reduced.vocab'.format(
        models_filepaths[0].split('.ppmi')[0])
    vocab = embeddix.load_vocab(vocab_filepath)
    splits_dicts = [entropix.load_splits_dict(DATASET, vocab, KFOLD_SIZE)
                    for _ in range(NUM_RUNS)]
    with MyPool(NUM_THREADS) as pool:
        _compute_results = functools.partial(
            utils.compute_window_seq_results, DATASET, splits_dicts,
            KFOLD_SIZE, METRIC, NUM_KFOLD_THREADS)
        for corpus, _results, _samples in pool.imap_unordered(_compute_results,
                                                              models_filepaths):
            results[corpus] = _results
            samples[corpus] = _samples
    pvals = utils.compute_win_pvals(models_filepaths, results, samples)
    print('-'*80)
    print('PRINT RESULTS ON {}'.format(DATASET.upper()))
    print('model & dim & alpha & {}'.format(' & '.join(
        [corpus.upper() for corpus in CORPUS_LIST])))

    print('TOP fair alpha=0 spr = {}'.format(
        ' & '.join(['{} $\\pm$ {}'
                    .format(results[corpus]['top-0']['avg'],
                            results[corpus]['top-0']['ste'])
                    for corpus in CORPUS_LIST])))
    print('SEQ spr = {}'.format(
        ' & '.join(['{} $\\pm$ {}'
                    .format(results[corpus]['seq']['spr']['avg'],
                            results[corpus]['seq']['spr']['ste'])
                    for corpus in CORPUS_LIST])))
    print('TOP fair alpha=0 pval = {}'.format(
        ' & '.join(['{}'.format(results[corpus]['top-0']['pval'])
                    for corpus in CORPUS_LIST])))
    print('SEQ dim = {}'.format(
        ' & '.join(['{} $\\pm$ {}'
                    .format(results[corpus]['seq']['dim']['avg'],
                            results[corpus]['seq']['dim']['ste'])
                    for corpus in CORPUS_LIST])))
    print('PRINT TOP CROSS P-VALUES')
    print('BEST & {}'.format(' & '.join(CORPUS_LIST)))
    for best in CORPUS_LIST:
        print('best-{} & {}'.format(best, ' & '.join(
            [str(pvals['top'][best][corr]) for corr in CORPUS_LIST])))
    print('PRINT SEQ CROSS P-VALUES')
    print('BEST & {}'.format(' & '.join(CORPUS_LIST)))
    for best in CORPUS_LIST:
        print('best-{} & {}'.format(best, ' & '.join(
            [str(pvals['seq'][best][corr]) for corr in CORPUS_LIST])))
